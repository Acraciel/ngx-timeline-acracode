# ngx-timeline-acracode
A timeline component for Angular 2+

## Install

```javascript
npm install ngx-timeline-acracode --save
```

## Component
- Selector：`ngx-timeline-acracode`

- Module `NgxTimelineAcracodeModule`

```javascript
import { NgxTimelineAcracodeModule } from 'ngx-timeline-acracode';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    NgxTimelineAcracodeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
```

- Example use:

```javascript
import { TimelineModel } from 'ngx-timeline-acracode';


events: Array<TimelineModel>;

ngOnInit() {
    this.events = new Array<TimelineModel>();
    this.events.push({ 'date': new Date(), 'header': 'Título 1', 'body': 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rem adipisci aliquam laudantium fugit, modi dicta impedit officiis, velit delectus alias ea officia quas rerum. Nisi, eius. Enim aut quibusdam at?Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rem adipisci aliquam laudantium fugit, modi dicta impedit officiis, velit delectus alias ea officia quas rerum. Nisi, eius. Enim aut quibusdam at?Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rem adipisci aliquam laudantium fugit, modi dicta impedit officiis, velit delectus alias ea officia quas rerum. Nisi, eius. Enim aut quibusdam at?Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rem adipisci aliquam laudantium fugit, modi dicta impedit officiis, velit delectus alias ea officia quas rerum. Nisi, eius. Enim aut quibusdam at?Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rem adipisci aliquam laudantium fugit, modi dicta impedit officiis, velit delectus alias ea officia quas rerum. Nisi, eius. Enim aut quibusdam at?','footer': 'Footer', 'icon': 'bitbucket', 'iconheadercolor':'#4e59f1' });
    this.events.push({ 'date': new Date(), 'header': 'Título 2', 'body': 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rem adipisci aliquam laudantium fugit, modi dicta impedit officiis, velit delectus alias ea officia quas rerum. Nisi, eius. Enim aut quibusdam at?','icon':'adn','iconheadercolor':'rgb(26, 181, 79)'});
    this.events.push({ 'date': new Date(), 'header': 'Título 3', 'body': 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rem adipisci aliquam laudantium fugit, modi dicta impedit officiis, velit delectus alias ea officia quas rerum. Nisi, eius. Enim aut quibusdam at?','iconheadercolor':'rgb(255, 25, 38)'});
    this.events.push({ 'date': new Date(), 'header': 'Título 4', 'body': 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rem adipisci aliquam laudantium fugit, modi dicta impedit officiis, velit delectus alias ea officia quas rerum. Nisi, eius. Enim aut quibusdam at?','icon': 'thumbs-up', 'iconheadercolor':'rgb(255, 164, 33)'});
    this.events.push({ 'date': new Date(), 'header': 'Título 5', 'body': 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rem adipisci aliquam laudantium fugit, modi dicta impedit officiis, velit delectus alias ea officia quas rerum. Nisi, eius. Enim aut quibusdam at?'});
  }
```

- Selector

```html
<ngx-timeline-acracode [events]="events"></ngx-timeline-acracode>
```

## Custom template
- `timelineDate` Hora personalizada
- `timelineHeader` Encabezado personalizado
- `timelineBody` Contenido personalizado
- `timelineFooter` Pie personalizado

**Example of use:**

```html
<ngx-timeline-acracode [events]="events">
    <ng-template let-event let-index="rowIndex" timelineBody>
        Contenido personalizado
    </ng-template>
</ngx-timeline-acracode>

```
** If you want get the data use the singular word of events ("event") **

## Models
- `events` = `TimelineModel` - Datos de la línea de tiempo
  - date: Date;
  - icon?: string;
  - iconheadercolor?: string;
  - header: string;
  - body: any; (Type Any, If you want to pass an object when you use a custom template)
  - footer?: string;

# Issues report

https://bitbucket.org/Acraciel/ngx-timeline-acracode/issues

# License
Released under the MIT License. See the [LICENSE](license) file for further details.