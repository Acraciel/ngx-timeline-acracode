import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NgxTimelineAcracodeModule } from 'ngx-timeline-acracode';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    NgxTimelineAcracodeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
