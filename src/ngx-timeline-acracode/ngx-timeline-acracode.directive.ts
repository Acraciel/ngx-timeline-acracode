import { Directive, TemplateRef } from '@angular/core';

// Time Line Date
@Directive({ selector: 'ng-template[timelineDate]' })
export class TimelineDate {
  constructor(public templateRef: TemplateRef<any>) { }
}

// Time Line Header
@Directive({ selector: 'ng-template[timelineHeader]' })
export class TimelineHeader {
  constructor(public templateRef: TemplateRef<any>) { }
}

// Time Line Body
@Directive({ selector: 'ng-template[timelineBody]' })
export class TimelineBody {
  constructor(public templateRef: TemplateRef<any>) { }
}

// Time Line Footer
@Directive({ selector: 'ng-template[timelineFooter]' })
export class TimelineFooter {
  constructor(public templateRef: TemplateRef<any>) { }
}
