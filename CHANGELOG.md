## 15/03/2018 ver. 2.1.2
- Mejoras de pruebas visuales.
- Se quitan propiedades que realmente van directamente en el cuerpo.
- Se le da la capacidad de cargar objetos en el cuerpo, no sólo texto.
## 15/03/2018 ver. 2.1.X
- Mejoras de pruebas visuales.
- Nuevas propiedades 
    - Soporte imagen
    - Soporte archivo
## 15/03/2018 ver. 2.0.0
- Cambio Visual Completo.
- Nuevo parámetro que configura el color para el header y el icono.

## 14/03/2018 ver. 1.0.X
- Inicialización del proyecto