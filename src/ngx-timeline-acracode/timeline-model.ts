export interface TimelineModel {
    date: Date;
    icon?: string;
    iconheadercolor?: string;
    header: string;
    body: any;
    footer?: string;
}
