import { Component } from '@angular/core';
import { TimelineModel } from 'ngx-timeline-acracode';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  events: Array<TimelineModel>;

  ngOnInit() {
    this.events = new Array<TimelineModel>();
    this.events.push({ 'date': new Date(), 'header': 'Título 1', 'body': 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rem adipisci aliquam laudantium fugit, modi dicta impedit officiis, velit delectus alias ea officia quas rerum. Nisi, eius. Enim aut quibusdam at?Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rem adipisci aliquam laudantium fugit, modi dicta impedit officiis, velit delectus alias ea officia quas rerum. Nisi, eius. Enim aut quibusdam at?Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rem adipisci aliquam laudantium fugit, modi dicta impedit officiis, velit delectus alias ea officia quas rerum. Nisi, eius. Enim aut quibusdam at?Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rem adipisci aliquam laudantium fugit, modi dicta impedit officiis, velit delectus alias ea officia quas rerum. Nisi, eius. Enim aut quibusdam at?Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rem adipisci aliquam laudantium fugit, modi dicta impedit officiis, velit delectus alias ea officia quas rerum. Nisi, eius. Enim aut quibusdam at?','footer': 'Footer', 'icon': 'bitbucket', 'iconheadercolor':'#4e59f1' });
    this.events.push({ 'date': new Date(), 'header': 'Título 2', 'body': 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rem adipisci aliquam laudantium fugit, modi dicta impedit officiis, velit delectus alias ea officia quas rerum. Nisi, eius. Enim aut quibusdam at?','icon':'adn','iconheadercolor':'rgb(26, 181, 79)'});
    this.events.push({ 'date': new Date(), 'header': 'Título 3', 'body': 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rem adipisci aliquam laudantium fugit, modi dicta impedit officiis, velit delectus alias ea officia quas rerum. Nisi, eius. Enim aut quibusdam at?','iconheadercolor':'rgb(255, 25, 38)'});
    this.events.push({ 'date': new Date(), 'header': 'Título 4', 'body': 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rem adipisci aliquam laudantium fugit, modi dicta impedit officiis, velit delectus alias ea officia quas rerum. Nisi, eius. Enim aut quibusdam at?','icon': 'thumbs-up', 'iconheadercolor':'rgb(255, 164, 33)','imageUrl':'https://thesettlersinn.com/wp-content/uploads/2014/05/SettlersInn-Room219-02-LARGE-IMG.jpg'});
    this.events.push({ 'date': new Date(), 'header': 'Título 5', 'body': 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Rem adipisci aliquam laudantium fugit, modi dicta impedit officiis, velit delectus alias ea officia quas rerum. Nisi, eius. Enim aut quibusdam at?','imageUrl':'https://i1.wp.com/www.redesoterica.net/wp-content/uploads/2016/09/p%C3%A1jaros.jpg?fit=1181%2C786','fileUrl':'https://www.alejandrodelasota.org/wp-content/uploads/2013/03/demoform1.pdf'});
  }
}
