import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxTimelineAcracodeComponent } from './ngx-timeline-acracode.component';
import { TimelineDate, TimelineHeader, TimelineBody, TimelineFooter } from './ngx-timeline-acracode.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    NgxTimelineAcracodeComponent,

    TimelineDate,
    TimelineHeader,
    TimelineBody,
    TimelineFooter
  ],
  exports: [
    NgxTimelineAcracodeComponent,

    TimelineDate,
    TimelineHeader,
    TimelineBody,
    TimelineFooter
  ]
})
export class NgxTimelineAcracodeModule { }
