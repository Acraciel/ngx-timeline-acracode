import { Component, OnInit, Input, Directive, TemplateRef, ContentChild } from '@angular/core';

import { TimelineModel } from './timeline-model';
import { TimelineDate, TimelineHeader, TimelineBody, TimelineFooter } from './ngx-timeline-acracode.directive';

@Component({
  selector: 'ngx-timeline-acracode',
  templateUrl: './ngx-timeline-acracode.component.html',
  styleUrls: ['./ngx-timeline-acracode.component.scss']
})
export class NgxTimelineAcracodeComponent implements OnInit {
  @Input() events: Array<TimelineModel>;
  @Input() timelineClass: string;
  @Input() timelineStyle: any;
  @Input() dateFormats = 'dd-MM-yyyy';

  @ContentChild(TimelineDate) dateTemplate: TimelineDate;
  @ContentChild(TimelineHeader) headerTemplate: TimelineHeader;
  @ContentChild(TimelineBody) bodyTemplate: TimelineBody;
  @ContentChild(TimelineFooter) footerTemplate: TimelineFooter;

  constructor() { }

  ngOnInit() {
  }

}
